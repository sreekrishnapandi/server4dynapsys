import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import redis.clients.jedis.Jedis;

class MasterPort implements Runnable {
	int slavePortIndex = 2005;
	String userID = "";
	String deviceID = "";
	Jedis slaveThread_idDB = null;

	Logger log = Logger.getLogger("MasterPort");

	@Override
	protected void finalize() throws Throwable {
		// close db if the master is terminated
		slaveThread_idDB.close();
		super.finalize();
	}

	public String readBR(BufferedReader br, int s) {
		long tStart = System.currentTimeMillis();
		// log.info("waiting for brREADY");
		while ((System.currentTimeMillis() - tStart) / 1000 < s) {
			try {
				if (br.ready()) {
					// log.info("reading line");
					return br.readLine();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return "NOIP";
	}

	MasterPort(int port) {
		this.slavePortIndex = port;
		log.setLevel(Level.INFO); // CHANGE LOG LEVEL to INFO to troubleshoot
	}

	@Override
	public void run() {
		log.info("Started MasterPort");
		ServerSocket srSockMaster = null;
		Socket sock = null;
		String rxString = null;
		BufferedReader bufferdSocketreader = null;
		PrintStream outputStream = null;

		// TODO: Make ip configurable
		slaveThread_idDB = new Jedis("127.0.0.1", 2000);

		while (true) {
			try {
				/*
				 * If encountered with BindException too frequently, consider
				 * moving the sock.Close() functions to the top.
				 */

				log.info("%%% Master - Iterate %%%");

				// TODO: Make port configurable
				// may throw IO-Exception
				srSockMaster = new ServerSocket(2002);

				/*
				 * //-----------------------###########################----------
				 * -------- System.out.println("Blah!!"); Scanner sc = new
				 * Scanner(System.in); String line = sc.next().toString();
				 * log.info("GOT FROM BOT!!! ::::> " + line);
				 */// -----------------------###########################------------------

				log.info("Master Waiting on port " + srSockMaster.getLocalPort());

				sock = srSockMaster.accept();

				log.info("Got Connection from " + sock.getInetAddress()	+ " at Port " + srSockMaster.getLocalPort());

				// may throw IO-Exception:
				bufferdSocketreader = new BufferedReader(new InputStreamReader(sock.getInputStream()));
				// may throw IO-Exception:
				outputStream = new PrintStream(sock.getOutputStream());

				rxString = readBR(bufferdSocketreader, 5);
				// log.info("RXSTRING :" + rxString);
				if (rxString != "NOIP" && rxString != null) {
					userID = rxString;
					log.info("USER ID = " + userID);

					rxString = readBR(bufferdSocketreader, 5);
					// log.info("RXSTRING :" + rxString);
					if (rxString != "NOIP" && rxString != null) {
						deviceID = rxString;
						log.info("DEVICE ID = " + deviceID);

						// TODO: Make port configurable
						if (slavePortIndex < 2049) {
							slavePortIndex++;
						} else {
							slavePortIndex = 2005;
						}

						Thread sT = new Thread(new SlavePort(slavePortIndex, userID, deviceID));
						sT.start();

						slaveThread_idDB.set(deviceID,String.valueOf(sT.getId()));
						outputStream.println(slavePortIndex);
						outputStream.flush();
					}
				}
				if (rxString == "NOIP") {
					log.warning("NOIP in Client ID");
				}

			} catch (IOException e1) {
			    e1.printStackTrace();
				log.severe(e1.getMessage());	
			}

			if (sock != null && sock.isClosed() == false) {
				try {
					sock.close();
					outputStream = null;
					srSockMaster.close();
					log.info("%%% MasterSock Closed %%%");
				} catch (IOException e) {
					e.printStackTrace();
					log.severe("Exception at CLOSE");
				}
			}
		}

	}

};
