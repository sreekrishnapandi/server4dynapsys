	import java.io.BufferedReader;
	import java.io.InputStreamReader;
	import java.net.HttpURLConnection;
	import java.net.URL;
	 
	 
	public class HttpURLConnectionExample {
	 
		private final String USER_AGENT = "Krish_TestCustomBrowser";
	 
		public static void main(String[] args) throws Exception {
	 
			HttpURLConnectionExample http = new HttpURLConnectionExample();
	 
			System.out.println("Testing 1 - Send Http GET request");
			http.sendGet();
	 
			//System.out.println("\nTesting 2 - Send Http POST request");
			//http.sendPost();
	 
		}
	 
		// HTTP GET request
		private void sendGet() throws Exception {
	 
			//String url = "http://141.76.50.92:8000/publish?content=bha";
			//String url = "http://141.76.50.92:8000/subscribe?id=test&filter=bha";
			String url = "http://141.76.50.92:8000/register?id=12";

			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	 
			// optional default is GET
			con.setRequestMethod("GET");
	 
			//add request header
			con.setRequestProperty("User-Agent", USER_AGENT);
	 
			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
	 
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			int i=0;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
				System.out.println(inputLine.toString());
				if (inputLine.length() == 0); //i'm done
//				System.out.println(++i);
//				System.out.println(inputLine.length());
//				System.out.println(response.toString());
			}
			in.close();
	 
			//print result
			//System.out.println(response.toString());
	 
		}		
	 
	
}
