import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import redis.clients.jedis.Jedis;

class SlavePort implements Runnable  {
	
	int port;
	Calendar cal;
	String userID = "";
	String deviceID = "";
	String threadID = "";
	Jedis messageDB = null;
	Jedis slaveThread_idDB = null;
	
	Logger log = Logger.getLogger("MySlaveLog");
	
	SlavePort(int port, String userID, String deviceID){
		this.port = port;
		this.userID = userID;
		this.deviceID = deviceID;
		
		
		log.setLevel(Level.INFO);									//CHANGE LOG LEVEL to INFO to troubleshoot
	}
	
	
	/**
	 * Appends URL depending on client's message.
	 * IMPORTANT : Permitted Client's message protocol as follows :
	 * P#<content>    	for publish	
	 * R#<id>			for Register
	 * S#<id>#<filter>	for Subscribe
	 * D#<id>#<content> 
	 * @param url
	 * @param input
	 * @return
	 * @throws MalformedURLException 
	 */
	URL urlAppend(String server, String input) throws MalformedURLException{
		String type = "";
		String id = "";
		String content="";
		String filter ="";
		
		//--Type Determination---
		switch(input.charAt(0)){
			case 'P':
				type = "publish";
				content = input.substring(2);
				break;
			case 'S':
				type = "subscribe";
				int index = input.indexOf('#', 2);
				id = input.substring(2, index);
				filter = input.substring(index+1);
				break;
			case 'R':
				type = "register";
				id = input.substring(2);
				break;
			case 'D':
				type = "direct";
				int index1 = input.indexOf('#', 2);
				id = input.substring(2, index1);
				content = input.substring(index1+1);
				break;
		}
		
		/*/--Parameter fixing---
		if(type == "register"){		
			id = input.substring(2);
		}else if(type == "subscribe"){
			int index = input.indexOf('#', 2);
			id = input.substring(2, index-1);
			filter = input.substring(2);
		}
		else if(type == "publish"){
			content = input.substring(2);
		}
		*/
		
		//--URL Appending----
		if (type=="subscribe"){			//subscribe?id=1&filter=.*"
			server = server + "subscribe?id=" + id + "&filter=" + filter;
			//log.info("URL : " + server);
			return new URL(server);
		}else if(type=="register"){
			server = server + "register?id=" + id ;
			return new URL(server);
		}else if(type=="direct"){
			server = server + "direct?id=" + id + "&content=" + content;
			//log.info("URL : " + server);
			return new URL(server);	
		}else if(type=="publish"){
			//publish?content=hello%20world"
			server = server + "publish?content=" + content;
			return new URL(server);
		}
		return new URL(server);						// Exception --- Handle later with better morals.
	
	 
	}
	
	
	@Override
	public void run() {
		this.threadID = String.valueOf(Thread.currentThread().getId());
		log.info("\nStarted Slave Port " + port);
		ServerSocket srSockSlave = null;
		try {		
			//log.info("Thread ID is : " + Thread.currentThread().getId());
			
			srSockSlave = new ServerSocket(port);
			log.info("SLAVE Waiting at "+ port);
			
			Socket sock = null;
			sock = srSockSlave.accept();
			log.info("\n Got Connection from "+sock.getInetAddress()+" at Slave Port " + port);
			
			//---For Communication With Client - 
			PrintStream ps = null;
			BufferedReader br = null;
			br = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			ps = new PrintStream(sock.getOutputStream());
			//--
			
			//---For Communication with the server ---
			String pubSub_Server = "http://141.76.50.92:8000/";
			HttpURLConnection primaryConnection, secondaryConnection;
			
			//--
			
			// Database connections
			messageDB = new Jedis("127.0.0.1", 2001);
			slaveThread_idDB = new Jedis("127.0.0.1", 2000);
			
			BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
			ps.println("ping from Server" + Calendar.getInstance().getTime());

			cal= Calendar.getInstance();
			long tStart = cal.getTimeInMillis();
			
			//--Setup- PrimaryConnection - Register and establish a BufferStream---
			URL primURL = urlAppend(pubSub_Server, ("R#"+userID));
			primaryConnection = (HttpURLConnection) primURL.openConnection();
			primaryConnection.setRequestMethod("GET");
			//int responseCode_P = primaryConnection.getResponseCode();
			BufferedReader pubBuffer = new BufferedReader(new InputStreamReader(primaryConnection.getInputStream()));
			//log.info("" + this.threadID + "====" + slaveThread_idDB.get(deviceID));
			while( false == ps.checkError() && this.threadID.equals(slaveThread_idDB.get(deviceID)) && (Calendar.getInstance().getTimeInMillis() - tStart) < 20000  ){
				//log.info("!!!Inside");
				if (messageDB.llen(deviceID) > 0){
					ps.println(messageDB.rpop(deviceID));
				}
				
				String line ="";
				if(pubBuffer.ready()){
					
					line = pubBuffer.readLine();
					
					if (line.length()<2 ) continue;
					log.info("$$$ HASNext-Server $$$ \nMSG : " + line);
					ps.println(line);
					ps.flush();
				}else if(br.ready()){
					line = br.readLine();
					
					if (line==null) break;			// Connection to client Broken.. cos br was NULL
					
					if(line.contentEquals("HeartBeat")){
						tStart = Calendar.getInstance().getTimeInMillis();
						//log.info("LUB-TUB");
					}
					else{
						log.info("$$$ HASNext-Client  $$$ DEVICE : " + this.deviceID + "\nMSG : " + line);
						//log.info("STRING : " + line);
						if (line.charAt(0)=='P'){
							URL secURL = new URL(pubSub_Server + "publish");
							secondaryConnection = (HttpURLConnection) secURL.openConnection();
							secondaryConnection.setRequestMethod("POST");
							String content = line.substring(2);
							String urlParameters = "content=" + content;
							 
							// Send post request
							secondaryConnection.setDoOutput(true);
							DataOutputStream wr = new DataOutputStream(secondaryConnection.getOutputStream());
							wr.writeBytes(urlParameters);
							wr.flush();
							wr.close();
							
							int responseCode = secondaryConnection.getResponseCode();
							System.out.println("\nSending 'POST' request to URL : " + secURL);
							System.out.println("Post parameters : " + urlParameters);
							System.out.println("Response Code : " + responseCode);
							
						}else if(line.charAt(0)=='D'){
							//System.out.println("DIRECT ");
							URL secURL = new URL(pubSub_Server + "direct");
							secondaryConnection = (HttpURLConnection) secURL.openConnection();
							secondaryConnection.setRequestMethod("POST");
							int index1 = line.indexOf('#', 2);
							String id = line.substring(2, index1);
							String content = line.substring(index1+1);
							
							String urlParameters = "id=" + id + "&content=" + content;
							
							
							 
							// Send post request
							secondaryConnection.setDoOutput(true);
							DataOutputStream wr = new DataOutputStream(secondaryConnection.getOutputStream());
							wr.writeBytes(urlParameters);
							wr.flush();
							wr.close();
							
							//int responseCode = secondaryConnection.getResponseCode();

							
						}else{
							URL secURL = urlAppend(pubSub_Server, line);
							secondaryConnection = (HttpURLConnection) secURL.openConnection();
							secondaryConnection.setRequestMethod("GET");
							//int responseCode_S = secondaryConnection.getResponseCode();
						}
						
					}
						
				}else{
					//log.info("$$$ NoNext  $$$");
					Thread.sleep(3000);
					
					/*if(checkHeartBeat>0)
						checkHeartBeat--;
					else{
						ps.println("CheckHeartBeat");
						log.info(String.valueOf(ps.checkError()));
						ps.flush();
						checkHeartBeat=2;
						log.info("CheckHeartBeat sent");
					}
					*/
					
				}
				
				/*
				tStart = cal.getTimeInMillis();
				
				while(cal.getTimeInMillis()<tStart+(1000*3)&& !ps.checkError())
					cal= Calendar.getInstance();
				*/
			}
			
			while(this.threadID.equals(slaveThread_idDB.get(deviceID))){ 		
				/**
				 * STATE: 	Device lost connection but not re-connected yet.. 
				 * ACTION: 	Continue retrieving messages from the PS. 
				 * 			and store it in a Data-store
				 */
				//log.info("In Datastore");
				String line ="";
				Thread.sleep(5000);
				if(pubBuffer.ready()){
					line = pubBuffer.readLine();
					if (line.length()<2 ) continue;
					messageDB.lpush(deviceID, line);
					log.info("$$$ HASNext-Server - DATASTORE  $$$ DEVICE : " + this.deviceID + "\nMSG : " + line );
				}
			}
			
			if(!this.threadID.equals(slaveThread_idDB.get(deviceID))){
				ps.println("KILLYOSELF");
				log.info("Sent KILLYOSELF to " + this.deviceID);
			}
				
			sock.close();
			srSockSlave.close();
			log.info("###################Closed Slave Port " + port+" ############################");
			
			
		} catch (IOException e) {
			e.printStackTrace();
			log.severe(e.getMessage());	
		} catch (InterruptedException e) {
			e.printStackTrace();
			log.severe(e.getMessage());	
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		messageDB.close();
		slaveThread_idDB.close();
		super.finalize();
	}
};
